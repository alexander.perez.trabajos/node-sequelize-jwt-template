import jwt from 'jsonwebtoken';
import path from 'path';
require('dotenv').config({path: path.resolve(__dirname, '../../.env')})

export const verifyAdmin = (req, res, next) => {
    const token = req.headers['auth-token'];
    if(!token){
        return res.status(403).json({ error: 'Acceso denegado' });
    }
    try {
        const verified = jwt.verify(token, process.env.TOKEN_SECRET);
        if(verified.role === 'admin'){
            req.user = verified;
            next();
        } else {
            res.sendStatus(403);
        }
    } catch (error) {
        res.sendStatus(403);
    }
}
