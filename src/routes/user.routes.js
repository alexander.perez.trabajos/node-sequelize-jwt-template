import { Router } from 'express';
const router = Router();

import * as controller from '../controllers/user.controller'

router.get('/', controller.getUsers);
router.get('/:id', controller.getUser);
// router.patch('/:id', controller.updateUser);
// router.delete('/:id', controller.deleteUser);

export default router;
