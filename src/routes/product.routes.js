import { Router } from 'express';
const router = Router();

import * as controller from '../controllers/product.controller';
import { verifyAdmin } from '../middlewares/validate-admin';

router.get('/', controller.getProducts);
router.get('/:id', controller.getProduct);
router.post('/', verifyAdmin, controller.createProduct);
router.patch('/:id', verifyAdmin, controller.updateProduct);
router.delete('/:id', verifyAdmin, controller.deleteProduct);


export default router;
