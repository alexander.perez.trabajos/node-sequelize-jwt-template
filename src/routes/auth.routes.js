import { Router } from 'express';
const router = Router();

import * as controller from '../controllers/auth.controller'

router.post('/register', controller.register);
router.post('/login', controller.login);

export default router;
