import app from './app';
import sequelize from './database';
import path from 'path';
require('dotenv').config({path: path.resolve(__dirname, '../.env')})

app.listen(app.get('port'), () => {
    console.log('listening on port:', app.get('port'));

    // Conectarse a la base de datos
    sequelize.sync({ force: false }).then(() => {
        console.log(`${process.env.DB_CONNECTION}: Database Connected`);
    })
        .catch(error => {
            console.log(`${process.env.DB_CONNECTION}: ${error}`)
        });
});
