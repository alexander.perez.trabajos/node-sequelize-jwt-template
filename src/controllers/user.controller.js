import { DataTypes } from 'sequelize';
import sequelize from '../database';

const User = require('../models/user')(sequelize, DataTypes);

export const getUser = (req, res) => {
    User.findOne({ where: { id: req.params.id, } })
        .then(user => {
            res.json(user);
        })
        .catch(error => res.json({ error }));
}

export const getUsers = (req, res) => {
    User.findAll()
        .then(users => {
            res.json(users);
        }).catch(error => res.json({ error }));
}


export const updateUser = (req, res) => {
    const validations = schemaRegister.validate(req.body);
    const emailExist = User.findOne({ where: { email: req.body.email, id: { [sequelize.Op.not]: req.params.id } } });

    if (validations.error) {
        return res.status(400).json({ error: validations.error.details[0].message });
    }
    if (emailExist) {
        return res.status(400).json({ error: 'Email already in use' });
    }

    User.update({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
    }, {
        where: { id: req.params.id, },
    })
        .then(() => {
            res.sendStatus(200)
        })
        .catch(error => res.json({ error }));
}

export const deleteUser = (req, res) => {
    User.destroy({ where: { id: req.params.id } })
        .then(() => {
            res.sendStatus(200);
        })
        .catch(error => res.json({ error }));
}


