import { DataTypes } from 'sequelize';
import sequelize from '../database';
import Joi from 'joi';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';

const path = require('path');
require('dotenv').config({ path: path.resolve(__dirname, '../../.env') });
const User = require('../models/user')(sequelize, DataTypes);

const schemaRegister = Joi.object({
    name: Joi.string().min(3).max(100).required(),
    email: Joi.string().max(255).required().email(),
    password: Joi.string().min(8).max(30).required(),
});

const schemaLogin = Joi.object({
    email: Joi.string().max(255).required().email(),
    password: Joi.string().min(8).max(30).required(),
});

export const register = async (req, res) => {
    const validations = schemaRegister.validate(req.body);
    const emailExist = await User.findOne({ where: { email: req.body.email } });

    if (validations.error) {
        return res.status(400).json({ error: validations.error.details[0].message });
    }
    if (emailExist) {
        return res.status(400).json({ error: 'Email already in use' });
    }

    User.create({
        name: req.body.name,
        email: req.body.email,
        password: User.generateHash(req.body.password),
    })
        .then(user => {
            res.status(200).json(user);
        })
        .catch(error => res.json({ error }));
}

export const login = async (req, res) => {
    const validations = schemaLogin.validate(req.body);
    if (validations.error) {
        return res.status(400).json({ error: validations.error.details[0].message });
    }

    const user = await User.findOne({ where: { email: req.body.email } });
    if (!user) return res.status(400).json({ error: 'User not found' });

    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if (!validPassword) return res.status(400).json({ error: 'Incorrect password' });

    const token = jwt.sign({
        id: user.id,
        name: user.name,
        email: user.email,
        role: user.role,
    }, process.env.TOKEN_SECRET);

    res.header('auth-token', token).json({ token });
}
