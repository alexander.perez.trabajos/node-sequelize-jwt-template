import { DataTypes } from 'sequelize';
import sequelize from '../database';
import Joi from 'joi';

const Product = require('../models/product')(sequelize, DataTypes);

const schemaProduct = Joi.object({
    title: Joi.string().max(255).required(),
    description: Joi.string().max(255),
    qty: Joi.string().max(50),
    price: Joi.number().max(10000000).required(),
    img: Joi.string().max(255),
    status: Joi.bool(),
});

export const getProducts = (req, res) => {
    Product.findAll()
        .then(products => {
            res.status(200).json(products);
        })
        .catch(error => res.json({ error }));
}

export const getProduct = (req, res) => {
    Product.findOne({ where: { id: req.params.id }})
        .then(product => {
            res.status(200).json(product);
        })
        .catch(error => res.json({ error }));
}

export const createProduct = (req, res) => {
    const validations = schemaProduct.validate(req.body);
    if (validations.error) {
        return res.status(400).json({ error: validations.error.details[0].message });
    }

    Product.create({
        title: req.body.title,
        description: req.body.description,
        qty: req.body.qty,
        price: req.body.price,
        img: req.body.img,
        status: req.body.status
    })
        .then(product => {
            res.status(200).json(product);
        })
        .catch(error => res.json({ error }));
}

export const updateProduct = async (req, res) => {
    const validations = schemaProduct.validate(req.body);
    const productExist = await Product.findOne({ where: { id: req.params.id } });

    if (validations.error) {
        return res.status(400).json({ error: validations.error.details[0].message });
    }
    if (productExist) {
        Product.update({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
        }, {
            where: { id: req.params.id, },
        })
            .then(() => {
                res.sendStatus(200)
            })
            .catch(error => res.json({ error }));
    } else {
        res.json({ error: 'Este producto no existe' });
    }
}

export const deleteProduct = async (req, res) => {
    const productExist = await Product.findOne({ where: { id: req.params.id } });
    if(productExist){
        Product.destroy({ where: { id: req.params.id }})
            .then(() => {
                res.sendStatus(200)
            })
            .catch(error => res.json({ error }));
    } else {
        res.json({ error: 'Este producto no existe' });
    }
}
