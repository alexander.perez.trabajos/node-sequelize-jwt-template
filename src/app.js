import express from 'express';
import morgan from 'morgan';
import path from 'path';
import cors from 'cors';
require('dotenv').config({ path: path.resolve(__dirname, '../.env') });

import userRoutes from './routes/user.routes';
import authRoutes from './routes/auth.routes';
import productsRoutes from './routes/product.routes';

const app = express();
const corsOptions = {
    origin: process.env.CORS_ORIGIN_URL,
    optionsSuccessStatus: 200
}

// Settings
app.set('port', process.env.PORT || 9000);

// Middlewares
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors(corsOptions));

// Routes
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '/views/welcome.html'))
});

app.use('/api/auth', authRoutes);
app.use('/api/users', userRoutes);
app.use('/api/products', productsRoutes);

export default app;
