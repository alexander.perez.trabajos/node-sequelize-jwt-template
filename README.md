# Node Sequelize JWT
### Made with node, sequelize and jwt to create a easily and fast backend

<br />
<br />

## Migrate database
if you want to migrate your database:
```
npm run migrate
```
or:
```
npm run migrate:fresh
```
if you want to restart your database.

## Make models and migrations

In order to create a model and a migration, type:
```
npm run make:model --name=<model_name> --attributes=<model_attributes>
```
For example:
```
npm run make:model --name=Product --attributes=name:string,price:integer
```
